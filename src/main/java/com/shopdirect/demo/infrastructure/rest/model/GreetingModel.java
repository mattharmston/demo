package com.shopdirect.demo.infrastructure.rest.model;

public class GreetingModel {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
