package com.shopdirect.acceptancetest.configuration;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.shopdirect" })
public class TestConfiguration {

}